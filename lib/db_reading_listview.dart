import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserInformation extends StatefulWidget {
  const UserInformation({Key? key}) : super(key: key);

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  final Stream<QuerySnapshot> _usersStream =
      FirebaseFirestore.instance.collection('users').snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        return Flexible(
            child: ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            return ListTile(
                onTap: () {},
                leading: const Icon(Icons.picture_in_picture),
                title: Text(data['first_name']),
                subtitle: Text(data['email_address']),
                trailing: TextButton(
                  child: const Text('Delete'),
                  style: TextButton.styleFrom(
                      textStyle: const TextStyle(fontSize: 12)),
                  onPressed: () {
                    FirebaseFirestore.instance.runTransaction(
                        (transaction) async =>
                            transaction.delete(document.reference));
                  },
                ));
          }).toList(),
        ));
      },
    );
  }
}
