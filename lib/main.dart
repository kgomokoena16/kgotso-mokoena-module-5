import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';
import 'db_reading_listview.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Kgotso_Mokoena_Module_5',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const DbaseCon(title: 'Connect to Firebase and CRUD'),
    );
  }
}

class DbaseCon extends StatefulWidget {
  const DbaseCon({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _DbaseConState createState() => _DbaseConState();
}

class _DbaseConState extends State<DbaseCon> {
  final emailfield = TextEditingController();
  final firstnamefield = TextEditingController();
  final lastnamefield = TextEditingController();

  @override
  void dispose() {
    emailfield.dispose();
    firstnamefield.dispose();
    lastnamefield.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text('Connect to Firebase and perform CRUD operations'),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20),
              child: TextField(
                controller: emailfield,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                    hintText: 'Enter valid email e.g abc@gmail.com'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: firstnamefield,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'First Name',
                    hintText: 'Enter first name'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                controller: lastnamefield,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                    hintText: 'Enter last name'),
              ),
            ),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: TextButton(
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection('users')
                      .add({
                        'email_address': emailfield.text.toString(),
                        'first_name': firstnamefield.text.toString(),
                        'last_name': lastnamefield.text.toString()
                        // ignore: avoid_print
                      })
                      .then((value) => print('successfully added user'))
                      // ignore: avoid_print
                      .catchError((error) => print('User not added $error'));
                  emailfield.clear();
                  firstnamefield.clear();
                  lastnamefield.clear();
                },
                child: const Text(
                  'Create',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            const Padding(
                padding: EdgeInsets.all(20),
                child: Divider(
                  height: 20,
                  color: Colors.blue,
                )),
            const UserInformation()
          ],
        ));
  }
}
